//
//  MapTests.swift
//  MapTests
//
//  Created by Jing Yen Tee on 11/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import XCTest
import CoreLocation
import MapKit

@testable import Map

class MapTests: XCTestCase {
    var viewModel: MapVM!
    
    override func setUp() {
        super.setUp()
        
        viewModel = MapVM()
        viewModel.userLocation = CLLocation(latitude: 3.146742, longitude: 101.696312)
    }
    
    override func tearDown() {
        viewModel = nil
        
        super.tearDown()
    }
    
    func testModelPointOfInterest() {
        let poi = PointOfInterest(location: CLLocation(latitude: 123, longitude: 456) , name: "Name")
        
        XCTAssertNotNil(poi.location, "Point of interest location shouldn't be nil")
        XCTAssertNotNil(poi.name, "Point of interest name shouldn't be nil")
        
        XCTAssertEqual(poi.location.coordinate.latitude, 123, "Point of interest latitude should be 123")
        XCTAssertEqual(poi.location.coordinate.longitude, 456, "Point of interest latitude should be 456")
        XCTAssertEqual(poi.name, "Name", "Point of interest name should be Name")
    }
    
    func testGenerateMapAnnotation() {
        let pointOfInterests = [ PointOfInterest(location: CLLocation(latitude: 3.134436, longitude: 101.686010), name: "KL Sentral")
        ]
        
        XCTAssertTrue(viewModel.generateMapAnnotations(places: pointOfInterests).count > 0, "Should receive at least one annotation object in array")
    }
    
    func testGeneratePointOfInterest() {
        let validMapItem = [
            MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2DMake(3.118326, 101.677347)))
        ]
        let emptyMapItem = [MKMapItem]()
        XCTAssertNotNil(viewModel.generatePointOfInterestBy(mapItems: validMapItem), "Point of interest shold not be nil")
        XCTAssertNil(viewModel.generatePointOfInterestBy(mapItems: emptyMapItem), "Point of interest should be nil")
    }
    
    func testSearchAirport() {
        let expect = expectation(description: "MKLocalSearch for airport")
        
        viewModel.searchAirport { (_) in
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 5) { (error) in
            print("Error: \(error?.localizedDescription ?? " unknown")")
            
        }
    }
    
}
