//
//  Distance.swift
//  Map
//
//  Created by Jing Yen Tee on 11/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import CoreLocation

protocol Distance {
    func distanceWithUser(userLocation: CLLocation) -> String
}
