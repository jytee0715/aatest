//
//  LocationManager.swift
//  Map
//
//  Created by Jing Yen Tee on 10/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationManagerDelegate {
    func didUpdateLocations(_ location: CLLocation)
}

extension LocationManagerDelegate {
    func didUpdateLocations(_ location: CLLocation) {
        
    }
}

class LocationManager: CLLocationManager {
    static let shared = LocationManager()
    
    var lmDelegate: LocationManagerDelegate?
    
    override init() {
        super.init()
        
        delegate = self
        desiredAccuracy = kCLLocationAccuracyBest
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            requestLocation()
        } else if status == .notDetermined {
            requestWhenInUseAuthorization()
        } else {
            let alert = UIAlertController(title: "Location Permission Required", message: "App does not has your phone's location permission access, please enable it in the setting", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "Go to setting", style: .default) { (action) in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                 }
            }
            alert.addAction(action)
            
            let window = UIApplication.shared.windows.first { $0.isKeyWindow }
            window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            lmDelegate?.didUpdateLocations(location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error: \(error)")
    }
}
