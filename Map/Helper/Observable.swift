//
//  Observable.swift
//  Map
//
//  Created by Jing Yen Tee on 11/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import Foundation

class Observable<T> {
    //MARK:- Properties
    var value: T? {
        didSet {
            guard let value = self.value else { return }
            self.valueChanged?(value)
        }
    }
    
    private var valueChanged: ((T) -> Void)?
    
    init(value: T? = nil) {
        self.value = value
    }
    
    //MARK:- Functions
    func addObserver(fireNow: Bool = false, onChange: ((T) -> Void)?) {
        valueChanged = onChange
        
        if let value = self.value, fireNow {
            onChange?(value)
        }
    }
    
    func removeObserver() {
        valueChanged = nil
    }
    
}
