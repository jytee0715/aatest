//
//  MapVw.swift
//  Map
//
//  Created by Jing Yen Tee on 11/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import Foundation
import MapKit

class MapVw: MKMapView {
    //MARK:- Initialization
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        delegate = self
        showsUserLocation = true
        isZoomEnabled = true
    }
}

extension MapVw: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        
        let identifier = "pin"
        let vw: MKAnnotationView? = {
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
            if view == nil {
                view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            
            let btn: UIButton = {
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                button.setBackgroundImage(UIImage(named: "airplane"), for: .normal)
                
                return button
            }()
            
            view?.canShowCallout = true
            view?.leftCalloutAccessoryView = btn
            view?.annotation = annotation
            view?.displayPriority = .required
            
            return view
        }()
        
        return vw
    }
    
}
