//
//  MapVC.swift
//  Map
//
//  Created by Jing Yen Tee on 09/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {
    //MARK:- Properties
    
    @IBOutlet weak var mapVw: MapVw!
    @IBOutlet weak var loadingVw: UIView! {
        didSet {
            loadingVw.layer.cornerRadius = 10
        }
    }
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnSearch: UIButton! {
        didSet {
            btnSearch.backgroundColor = .systemRed
            btnSearch.setTitle("Awaiting Location Update", for: .normal)
            btnSearch.layer.shadowColor = UIColor.black.cgColor
            btnSearch.layer.shadowOffset = CGSize(width: 0.1, height: 0.1)
            btnSearch.layer.shadowOpacity = 1.0
            btnSearch.addTarget(self, action: #selector(searchAirport), for: .touchUpInside)
        }
    }
    
    lazy var viewModel = {
        return MapVM()
    }()
    
    internal lazy var focusUserOnce: Void = {
        let region = MKCoordinateRegion(center: viewModel.userLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapVw.setRegion(region, animated: true)
    }()
    
    //MARK:- View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initField()
        initVw()
        initVM()
    }
    
    //MARK:- Initialization
    func initField() {
        
    }
    
    func initVw() {
        btnSearch.layer.cornerRadius = 8
    }
    
    func initVM() {
        viewModel.enableButtonChanged.addObserver {[weak self] (enable) in
            if enable {
                self?.btnSearch.isUserInteractionEnabled = true
                self?.btnSearch.backgroundColor = .white
                self?.btnSearch.setTitle("Search Airport", for: .normal)
            }
        }
        
        viewModel.isLoading.addObserver {[weak self] (isLoading) in
            self?.loadingVw.isHidden = !isLoading
        }
        
        viewModel.userLocationChanged.addObserver {[weak self] (userLocation) in
            if let _ = userLocation {
                _ = self?.focusUserOnce
            }
        }
        
        viewModel.annotionChanged.addObserver(onChange: {[unowned self]  (annotations) in
            if let annotations = annotations {
                self.mapVw.removeAnnotations(self.mapVw.annotations)
                self.mapVw.addAnnotations(annotations)
                self.mapVw.showAnnotations(self.mapVw.annotations, animated: true)
            } else {
                self.mapVw.removeAnnotations(self.mapVw.annotations)
                self.mapVw.addAnnotation(self.mapVw.userLocation)
                self.mapVw.showAnnotations(self.mapVw.annotations, animated: true)
            }
        })
    }
    
    //MARK:- Functions
    @objc func searchAirport() {
        viewModel.getAirportAnnotaions()
    }
}



