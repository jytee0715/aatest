//
//  MapVM.swift
//  Map
//
//  Created by Jing Yen Tee on 10/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import Foundation
import MapKit


class MapVM {
    //MARK:- Properties
    let locationManager = LocationManager.shared
    
    var userLocation: CLLocation!
    private var currentAirports: [PointOfInterest]?
    
    //MARK:- Reactive Properties
    let isLoading = Observable<Bool>(value: false)
    let annotionChanged = Observable<[MKPointAnnotation]?>(value: nil)
    let userLocationChanged = Observable<CLLocation?>(value: nil)
    let enableButtonChanged = Observable<Bool>(value: false)
    
    internal lazy var enableBtnOnceCall: Void = {
        enableButtonChanged.value = true
        isLoading.value = false
    }()
    
    //MARK:- Initialization
    init() {
        locationManager.lmDelegate = self
        locationManager.startUpdatingLocation()
    }
    
    //MARK:- Functions
    func getAirportAnnotaions() {
        isLoading.value = true
        
        searchAirport {[weak self] (mapItems) in
            if let mapItems = mapItems {
                self?.currentAirports = self?.generatePointOfInterestBy(mapItems: mapItems)
            } else {
                self?.currentAirports = nil
            }
            
            if let currentAirports = self?.currentAirports {
                if let annotaions = self?.generateMapAnnotations(places: currentAirports) {
                    self?.annotionChanged.value = annotaions
                } else {
                    self?.annotionChanged.value = nil
                }
            }
            
            self?.isLoading.value = false
        }
    }
    
    func generateMapAnnotations(places: [PointOfInterest]) -> [MKPointAnnotation] {
        var annotations = [MKPointAnnotation]()
        
        for place in places {
            let annotation = MKPointAnnotation()
            annotation.coordinate = place.location.coordinate
            annotation.title = place.name
            annotation.subtitle = place.distanceWithUser(userLocation: userLocation)
            
            annotations.append(annotation)
        }
        
        return annotations
    }
    
    func generatePointOfInterestBy(mapItems: [MKMapItem]) -> [PointOfInterest]? {
        var pointOfInterests = [PointOfInterest]()
        
        for item in mapItems {
            guard let location = item.placemark.location else { continue }
            
            let obj = PointOfInterest(location: location, name: item.placemark.name)
            pointOfInterests.append(obj)
        }
        
        if pointOfInterests.count > 0 {
            return pointOfInterests
        } else {
            return nil
        }
    }
    
    func searchAirport(completion: @escaping (([MKMapItem]?) -> Void)) {
        let request = MKLocalSearch.Request()
        request.pointOfInterestFilter = .some(MKPointOfInterestFilter(including: [.airport]))
        request.naturalLanguageQuery = "airport"
        
        let search = MKLocalSearch(request: request)
        search.start { response, error in
            if let response = response, response.mapItems.count > 0 {
                completion(response.mapItems)
            } else {
                completion(nil)
            }
        }
    }
}

extension MapVM: LocationManagerDelegate {
    func didUpdateLocations(_ location: CLLocation) {
        userLocation = location
        _ = enableBtnOnceCall
        userLocationChanged.value = userLocation
    }
}
