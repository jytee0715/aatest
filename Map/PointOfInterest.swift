//
//  PointOfInterest.swift
//  PointOfInterest
//
//  Created by Jing Yen Tee on 10/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import CoreLocation

struct PointOfInterest {
    var location: CLLocation!
    var name: String?
}

extension PointOfInterest: Distance {
    func distanceWithUser(userLocation: CLLocation) -> String {
        let dis = userLocation.distance(from: location)
        return String(format: "\(Int(ceil(dis / 1000))) KM")
    }
}
