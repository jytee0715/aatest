//
//  MKMapView.swift
//  Map
//
//  Created by Jing Yen Tee on 10/01/2020.
//  Copyright © 2020 Jing Yen Tee. All rights reserved.
//

import MapKit

extension MKMapView {
    func addPinToMapVw(annotations: [MKAnnotation]) {
        showAnnotations(annotations, animated: true)
    }
}
